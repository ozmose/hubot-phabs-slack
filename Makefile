# REPORTER = progress
# REPORTER = list
# REPORTER = spec
# REPORTER = dot

test-full:
	@NODE_ENV=test ./node_modules/.bin/mocha \
		--require coffeescript/register \
		--require ./coffee-coverage.js \
		--reporter dot \
		--ui tdd \
		test/* \
		&& ./node_modules/.bin/coffeelint test/* scripts/* lib/* index.coffee \
		&& ./node_modules/.bin/istanbul report

test:
	@NODE_ENV=test ./node_modules/.bin/mocha \
		--require coffeescript/register \
		--reporter dot \
		--ui tdd \
		test/*

test-spec:
	@NODE_ENV=test ./node_modules/.bin/mocha \
		--require coffeescript/register \
		--reporter spec \
		--ui tdd \
		test/*

test-w:
	@NODE_ENV=test ./node_modules/.bin/mocha \
		--require coffeescript/register \
		--reporter min \
		--ui tdd \
		--watch \
		test/*

test-coverage:
	@NODE_ENV=test ./node_modules/.bin/mocha \
		--require coffeescript/register \
		--require coffee-coverage/register-istanbul \
		--reporter dot \
		--ui tdd \
		test/* \
		&& ./node_modules/.bin/istanbul report lcovonly

test-cov:
	@NODE_ENV=test ./node_modules/.bin/mocha \
		--require coffeescript/register \
		--require coffee-coverage/register-istanbul \
		--reporter dot \
		--ui tdd \
		test/* \
		&& ./node_modules/.bin/istanbul report text-summary

lint:
	@NODE_ENV=test ./node_modules/.bin/coffeelint test/* scripts/* lib/* index.coffee

sloc:
	@NODE_ENV=test ./node_modules/.bin/sloc --details \
       --format cli-table \
       --keys total,source,comment \
       --exclude i18n*.\.coffee \
       scripts/ lib/ test/

.PHONY: test test-spec test-w test-coverage test-cov test-full lint sloc
